// main.cpp

#include <fstream>
#include <iostream>
#include <ctime>

using namespace std;

class Abstract {
public:
    Abstract(const int & i, const float & f, const char & c) {
        this->i = i;
        this->f = f;
        this->c = c;
    }
    int getInteger() const { return i; }
    float getReal() const { return f; }
    char getChar() const { return c; }
    
    void setInteger(const int & i) { this->i = i; }
    void setFloat(const float & f) { this->f = f; }
    void setChar(const char & c) { this->c = c; }
private:
    int i;
    float f;
    char c;
};

inline short getRandomizedShort(const short & b, const short & e) {
    return rand() % (e - b + 1) + b;
}

Abstract ** Randomize(const size_t & count) {
    const short RANGE_BEGIN = 1;
    const short RANGE_END = 10000;
    
    Abstract ** arr = new Abstract*[count];
    
    for (size_t i = 0; i < count; ++i) {
        arr[i] = new Abstract(getRandomizedShort(RANGE_BEGIN, RANGE_END),
                              static_cast<char>(getRandomizedShort('A', 'Z')),
                              100.0f + i + 1);
    }
    return arr;
}

void Erase(Abstract ** arr, const size_t & size) {
    for (size_t i = 0; i < size; ++i) {
        delete arr[i];
    }
    delete[] arr;
}

void Sort(Abstract ** arr, const size_t & size) {
    for (size_t i = 0; i < size - 1; ++i) {
        bool flag = false;
        for (size_t j = 0; i < size - i - 1; ++j) {
            if (arr[j]->getInteger() > arr[j+1]->getInteger()) {
                swap(arr[j], arr[j+1]);
                flag = true;
            }
        }
        if (!flag) {
            break;
        }
    }
}

size_t CountCharAppearences(const Abstract ** arr, const size_t & size, const char & c) {
    size_t count = 0U;
    
    for (size_t i = 0; i < size; ++i) {
        if (arr[i]->getChar() == c) {
            ++count;
        }
    }
    return count;
}

int main()
{
    srand(static_cast<unsigned>(time(0)));
    clock_t start = clock();
    
    ifstream inputFile("inlab01.txt", ios::in);
    if (inputFile.good()) {
        size_t count;
        char c;
        
        inputFile >> count >> c;
        Abstract ** arr = Randomize(count);
        Sort(arr, count);
    } else {
        cout << "Couldn't open text file." << endl;
    }
    size_t executionTimeInMilliseconds = (clock() - start) / CLOCKS_PER_SEC;
    cout << "Program's execution time: " << executionTimeInMilliseconds << " ms.";
    
    cin.get();
    return 0;
}
