// bst.h

#ifndef bst_h
#define bst_h

struct Node;
enum class State;
typedef unsigned long size_t;

class BinarySearchTree {
public:
    BinarySearchTree();
    BinarySearchTree(const BinarySearchTree&) = delete;
    BinarySearchTree& operator=(const BinarySearchTree&) = delete;
    ~BinarySearchTree();
    
    State Insert(const int&);
    void InsertFew(const int&);
    Node* Search(const int&) const;
    State Delete(const int&);
    void ShowPreorder() const;
    void ShowInorder() const;
    void ShowPostorder() const;
    void Balance();
    State Cleanup();
    size_t CalculateHeight() const;
    
private:
    Node* root_;
    size_t numberOfNodes_;
    
    static const int UPPER_RANDOMIZATION_BRINK = 19000;
    static const int LOWER_RANDOMIZATION_BRINK = 11;
    
    void leftRotate(Node*, Node*, Node*);
    void rightRotate(Node*, Node*, Node*);
    void makeIntermediateList();
    void makePerfectTree();
    
    inline Node* getTreeMaximum(Node*) const;
    inline Node* getTreeMinimum(Node*) const;
    Node* getParent(Node*) const;
};
#endif
