// main.cpp

#include "bst.h"
#include <iostream>
#include <fstream>
#include <chrono>

int main()
{
    using namespace std;
    using namespace chrono;
    
    ifstream inputFile("inlab04.txt");
    if (inputFile.is_open()) {
        int x[2] {};
        for (int& i : x) {
            inputFile >> i;
        }
        srand(static_cast<unsigned>(time(0)));
        
        auto t1 = high_resolution_clock::now();
        BinarySearchTree* bst = new BinarySearchTree();
        bst->InsertFew(x[0]);
        cout << "Aktualna wysokosc drzewa: " << bst->CalculateHeight() << endl;
        bst->Balance();
        cout << "Aktualna wysokosc drzewa: " << bst->CalculateHeight() << endl;
        bst->Cleanup();
        bst->InsertFew(x[1]);
        cout << "Aktualna wysokosc drzewa: " << bst->CalculateHeight() << endl;
        bst->Balance();
        cout << "Aktualna wysokosc drzewa: " << bst->CalculateHeight() << endl;
        auto t2 = high_resolution_clock::now();
        cout << "Czas wykonania: " << duration_cast<milliseconds>(t2-t1).count() << " ms." << endl;
        delete bst;
    } else {
        cout << "Nie udalo sie otworzyc pliku.\n";
    }
    return 0;
}
