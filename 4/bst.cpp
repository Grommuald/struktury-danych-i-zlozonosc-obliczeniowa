// bst.cpp

#include "util.h"
#include "bst.h"
using std::cout;
using std::endl;

const int BinarySearchTree::UPPER_RANDOMIZATION_BRINK;
const int BinarySearchTree::LOWER_RANDOMIZATION_BRINK;

BinarySearchTree::BinarySearchTree() : root_{nullptr}, numberOfNodes_(0U) {
    
}
BinarySearchTree::~BinarySearchTree() {
    Cleanup();
}

State BinarySearchTree::Insert(const int& key) {
    if (!root_) {
        root_ = new Node(key);
    } else {
        Node* current = root_;
        bool found = false;
        
        auto handleNode = [&] (Node*& next) {
            if (!next) {
                next = new Node(key);
                found = true;
            } else {
                current = next;
            }
            
        };
        while (!found) {
            if (key > current->key) {
                handleNode(current->right);
            } else if (key < current->key){
                handleNode(current->left);
            } else {
                return State::Failure;
            }
        }
    }
    ++numberOfNodes_;
    return State::Success;
}

void BinarySearchTree::InsertFew(const int& x) {
    RandomInt random(LOWER_RANDOMIZATION_BRINK, UPPER_RANDOMIZATION_BRINK);
    for (int i = 0; i < x; ++i) {
        Insert(random());
    }
}

State BinarySearchTree::Delete(const int& key) {
    if (!root_) {
        return State::Failure;
    }
    Node* wanted = root_;
    Node* parent = nullptr;
    bool found = false;
    
    auto clearPointer = [] (Node*& pointer) {
        if (pointer) {
            delete pointer;
            pointer = nullptr;
        }
    };
    
    auto setDescendants = [&] (Node* setTo) {
        if (parent->left == wanted) {
            parent->left = setTo;
            clearPointer(wanted);
        } else {
            parent->right = setTo;
            clearPointer(wanted);
        }
    };
    
    while (wanted && !found) {
        if (key == wanted->key) {
            found = true;
        } else {
            parent = wanted;
            if (key > wanted->key) {
                wanted = wanted->right;
            } else if (key < wanted->key) {
                wanted = wanted->left;
            }
        }
    }
    if (!wanted) {
        return State::Failure;
    }
    // leaf
    if (!wanted->left && !wanted->right) {
        if (wanted == root_) {
            clearPointer(root_);
        } else {
            setDescendants(nullptr);
        }
    } else if (!wanted->right) {
        if (wanted == root_) {
            Node* newroot_ = root_->left;
            delete root_;
            root_ = newroot_;
        } else {
            setDescendants(wanted->left);
        }
    } else if (!wanted->left) {
        if (wanted == root_) {
            Node* newroot_ = root_->right;
            delete root_;
            root_ = newroot_;
        } else {
            setDescendants(wanted->right);
        }
    } else {
        // 2nd degree subtree
        Node* successor = wanted->right;
        Node* parentOfSuccessor = wanted;
        Node* minimumNode = getTreeMinimum(wanted->right);
        
        if (successor != minimumNode) {
            while (successor != minimumNode) {
                parentOfSuccessor = successor;
                successor = successor->left;
            }
        }
        if (wanted == root_) {
            if (successor->right) {
                parentOfSuccessor->left = successor->right;
            } else {
                parentOfSuccessor->left = nullptr;
            }
            successor->right = root_->right;
            successor->left = root_->left;
            clearPointer(root_);
            root_ = successor;
        } else {
            if (successor->right) {
                parentOfSuccessor->right = successor->right;
            } else {
                if (parentOfSuccessor != wanted) {
                    parentOfSuccessor->left = nullptr;
                }
            }
            if (parent->left == wanted) {
                parent->left = successor;
            } else {
                parent->right = successor;
            }
            successor->left = wanted->left;
            clearPointer(wanted);
        }
    }
    --numberOfNodes_;
    return State::Success;
}
State BinarySearchTree::Cleanup() {
    std::function<void(Node*&)> cleanup = [&] (Node*& current) {
        if (current->left) {
            cleanup(current->left);
        }
        if (current->right) {
            cleanup(current->right);
        }
        delete current;
        current = nullptr;
    };
    if (root_) {
        cleanup(root_);
        numberOfNodes_ = 0U;
        return State::Success;
    } else {
        return State::Failure;
    }
}
Node* BinarySearchTree::Search(const int& key) const {
    Node* current = root_;
    while (current) {
        if (key > current->key) {
            current = current->right;
        } else if (key < current->key) {
            current = current->left;
        } else {
            return current;
        }
    }
    return nullptr;
}
void BinarySearchTree::ShowPreorder() const {
    unsigned numberOfVisitedNodes = 0U;
    cout << "----------------------------------------------------------" << endl
    << "Wyswietlam drzewo w trybie preorder:\n";
    std::function<void(Node*, unsigned int&)> preorder_lambda = [&] (Node* current, unsigned int& numberOfVisitedNodes) {
        if (current) {
            cout << current->key << " ";
            ++numberOfVisitedNodes;
            preorder_lambda(current->left, numberOfVisitedNodes);
            preorder_lambda(current->right, numberOfVisitedNodes);
        }
    };
    preorder_lambda(root_, numberOfVisitedNodes);
    cout << "\nLiczba odwiedzonych wezlow: " << numberOfVisitedNodes << endl
    << "----------------------------------------------------------" << endl;
}
void BinarySearchTree::ShowInorder() const {
    unsigned numberOfVisitedNodes = 0U;
    cout << "----------------------------------------------------------" << endl
    << "Wyswietlam drzewo w trybie inorder:\n";
    std::function<void(Node*, unsigned int&)> inorder_lambda = [&] (Node* current, unsigned int& numberOfVisitedNodes) {
        if (current) {
            inorder_lambda(current->left, numberOfVisitedNodes);
            cout << current->key << " ";
            ++numberOfVisitedNodes;
            inorder_lambda(current->right, numberOfVisitedNodes);
        }
    };
    inorder_lambda(root_, numberOfVisitedNodes);
    cout << "\nLiczba odwiedzonych wezlow: " << numberOfVisitedNodes << endl
    << "----------------------------------------------------------" << endl;
}
void BinarySearchTree::ShowPostorder() const {
    unsigned numberOfVisitedNodes = 0U;
    cout << "----------------------------------------------------------" << endl
    << "Wyswietlam drzewo w trybie postorder:\n";
    std::function<void(Node*, unsigned int&)> postorder_lambda = [&] (Node* current, unsigned int& numberOfVisitedNodes) {
        if (current) {
            postorder_lambda(current->left, numberOfVisitedNodes);
            postorder_lambda(current->right, numberOfVisitedNodes);
            cout << current->key << " ";
            ++numberOfVisitedNodes;
        }
    };
    postorder_lambda(root_, numberOfVisitedNodes);
    cout << "\nLiczba odwiedzonych wezlow: " << numberOfVisitedNodes << endl
    << "----------------------------------------------------------" << endl;
}
Node* BinarySearchTree::getTreeMinimum(Node* x) const {
    while (x->left) {
        x = x->left;
    }
    return x;
}
Node* BinarySearchTree::getTreeMaximum(Node* x) const {
    while (x->right) {
        x = x->right;
    }
    return x;
}
Node* BinarySearchTree::getParent(Node* x) const {
    Node* parent = nullptr;
    Node* current = root_;
    
    while (current != x || current) {
        parent = current;
        if (x->key > current->key) {
            current = current->right;
        } else {
            current = current->left;
        }
    }
    return parent;
}
void BinarySearchTree::leftRotate(Node* grandparent, Node* parent, Node* child) {
    if (grandparent) {
        if (grandparent->left == parent) {
            grandparent->left = child;
        } else {
            grandparent->right = child;
        }
    } else {
        root_ = child;
    }
    Node* tmp = child->left;
    child->left = parent;
    parent->right = tmp;
}
void BinarySearchTree::rightRotate(Node* grandparent, Node* parent, Node* child) {
    if (grandparent) {
        if (grandparent->right == parent) {
            grandparent->right = child;
        } else {
            grandparent->left = child;
        }
    } else {
        root_ = child;
    }
    Node* tmp = child->right;
    child->right = parent;
    parent->left = tmp;
}
void BinarySearchTree::makeIntermediateList() {
    Node* grandparent = nullptr;
    Node* itr = root_;
    
    while (itr) {
        if (itr->left) {
            Node* tmp = itr->left;
            rightRotate(grandparent, itr, itr->left);
            itr = tmp;
        } else {
            grandparent = itr;
            itr = itr->right;
        }
    }
}
void BinarySearchTree::makePerfectTree() {
    Node* grandfather = nullptr;
    Node* tmp = root_;
    Node* tmp2 = nullptr;
    
    int m = 1;
    
    while (m <= numberOfNodes_) {
        m = 2*m + 1;
    }
    m /= 2;
    for (int i = 0; i < numberOfNodes_-m; ++i) {
        tmp2 = tmp->right;
        if (tmp2) {
            leftRotate(grandfather, tmp, tmp->right);
            grandfather = tmp2;
            tmp = tmp2->right;
        }
    }
    while (m > 1) {
        m /= 2;
        grandfather = nullptr;
        tmp = root_;
        for (int i = 0; i < m; ++i) {
            tmp2 = tmp->right;
            leftRotate(grandfather, tmp, tmp->right);
            grandfather = tmp2;
            tmp = tmp2->right;
        }
    }
}
void BinarySearchTree::Balance() {
    makeIntermediateList();
    makePerfectTree();
}
size_t BinarySearchTree::CalculateHeight() const {
    std::function<size_t(Node*)> calculateHeight = [&](Node* node)->size_t {
        if (!node) {
            return 0;
        } else {
            return std::max(calculateHeight(node->left), calculateHeight(node->right)) + 1;
        }
    };
    return calculateHeight(root_);
}
