// util.h

#ifndef util_h
#define util_h

#include <iostream>
#include <ctime>
#include <vector>

struct Node {
    Node(const int& k) : key{k}, left{nullptr}, right{nullptr} {
        data = new char[DATA_SIZE];
        for (size_t i = 0U; i < Node::DATA_SIZE; ++i) {
            data[i] = static_cast<char>(rand() % 256);
        }
    }
    ~Node() {
        delete[] data;
    }
    static const size_t DATA_SIZE = 100;
    
    char* data;
    int key;
    Node* left;
    Node* right;
};

enum class State {
    Success,
    Failure
};

class RandomInt {
public:
    RandomInt() = delete;
    RandomInt(const int& a, const int& b) : begin{a}, end{b} {}
    int operator()() {
        bool foundAlreadyRandomized = true;
        int newRandom = 0;
        
        while (foundAlreadyRandomized) {
            newRandom = rand() % (end - begin + 1) + begin;
            auto i = alreadyRandomized.begin();
            for (; i != alreadyRandomized.end(); ++i) {
                if (*i == newRandom) {
                    break;
                }
            }
            if (i == alreadyRandomized.end()) {
                alreadyRandomized.push_back(newRandom);
                break;
            }
        }
        return newRandom;
    }
private:
    std::vector<int> alreadyRandomized;
    int begin, end;
};
#endif
