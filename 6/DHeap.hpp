// DHeap.hpp

#include "utils.h"

#ifndef DHeap_hpp
#define DHeap_hpp

class DHeap {
public:
    DHeap();
    DHeap(const DHeap&) = delete;
    DHeap& operator=(const DHeap&) = delete;
    ~DHeap();
    
    void Insert(const int&);
    int DeleteMaxNode();
    int DeleteMinNode();
    void InsertFew(const size_t&);
    void Cleanup();
    void ShowPreorder() const;
    inline bool IsEmpty() const { return IsMaxEmpty() && IsMinEmpty(); }
    inline bool IsMaxEmpty() const { return !root_->right; }
    inline bool IsMinEmpty() const { return !root_->left; }
    
private:
    Node* root_;
    size_t currentLevel_ {1};
    size_t levelIndex_ {0};
    size_t maxNumberOfNodes_ {2};
    static const int UPPER_RANDOMIZATION_BRINK;
    static const int LOWER_RANDOMIZATION_BRINK;
    
    int deleteHighestNode_(Node*&, const std::function<bool(const int&, const int&)>&);
    Node* findCousin_(const unsigned long&, unsigned long, unsigned long&);
    Node* findParent_(const unsigned long&, unsigned long);
};
#endif
