// main.cpp

#include "utils.h"
#include "DHeap.hpp"
#include <iostream>
#include <fstream>
#include <chrono>

int main(int argc, const char * argv[]) {
    using namespace std;
    using namespace chrono;
    
    ifstream inputFile("inlab06.txt");
    if (inputFile.is_open()) {
        int x;
        inputFile >> x;
        
        srand(static_cast<unsigned>(time(0)));
        
        auto t1 = high_resolution_clock::now();
        DHeap* dHeap = new DHeap();
        dHeap->InsertFew(x);
        dHeap->ShowPreorder();
        cout << "\n##############################################################"
                "\nElement o kluczu najwiekszym (" << dHeap->DeleteMaxNode() << ") zostal usuniety z kopca."
                "\n##############################################################\n" << endl;
        dHeap->ShowPreorder();
        cout << "\n##############################################################"
                "\nROZPOCZYNAM USUWANIE KOPCA..."
                "\n##############################################################" << endl;
        while (!dHeap->IsEmpty()) {
            if (!dHeap->IsMinEmpty()) {
                cout << "\n##############################################################"
                        "\nElement o kluczu najmniejszym (" << dHeap->DeleteMinNode() << ") zostal usuniety z kopca."
                        "\n##############################################################\n\n";
                dHeap->ShowPreorder();
            }
            if (!dHeap->IsMaxEmpty()) {
                cout << "\n###############################################################"
                        "\nElement o kluczu najwiekszym (" << dHeap->DeleteMaxNode() << ") zostal usuniety z kopca."
                        "\n###############################################################\n\n";
                dHeap->ShowPreorder();
            }
        }
        auto t2 = high_resolution_clock::now();
        cout << "Czas wykonania: " << duration_cast<milliseconds>(t2-t1).count() << " ms." << endl;
        delete dHeap;
    } else {
        cout << "Nie udalo sie otworzyc pliku.\n";
    }
    return 0;
}
