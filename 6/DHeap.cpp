// DHeap.cpp

#include "DHeap.hpp"
#include <cmath>
#include <string>

const int DHeap::UPPER_RANDOMIZATION_BRINK = 30000;
const int DHeap::LOWER_RANDOMIZATION_BRINK = 25;

DHeap::DHeap() : root_(new Node) {
    
}
DHeap::~DHeap() {
    Cleanup();
}
void DHeap::Insert(const int& key) {
    Node** current = &root_;
    
    // place node as a leaf at the end of the heap
    for (size_t i = maxNumberOfNodes_>>1; i; i>>=1) {
        if (levelIndex_ & i) {
            current = &((*current)->right);
        } else {
            current = &((*current)->left);
        }
    }
    *current = new Node(key);
    
    // promote the node in order to restore heap's integrity
    if (root_->right) {
        Node* curr = *current;
        unsigned long cousinsLevel = 0;
        unsigned long currLevel = currentLevel_;
        unsigned long levelIndex = levelIndex_;
        Node* cousin = findCousin_(currentLevel_, levelIndex_, cousinsLevel);
        Node* parent = findParent_(currentLevel_, levelIndex_);
        
        auto restoreHeapOrder = [&] (const std::function<bool(const int& a, const int& b)>& p) {
            while (parent != root_ && p(curr->key, parent->key)) {
                std::swap(curr->key, parent->key);
                curr = parent;
                parent = findParent_(currLevel-=1, levelIndex>>=1);
            }
            curr = cousin;
            currLevel = currentLevel_;
            levelIndex = levelIndex_;
            levelIndex ^= (maxNumberOfNodes_/2);
        };
        
        if (levelIndex_ < maxNumberOfNodes_/2) {
            // node has been created inside the min heap
            if (curr->key > cousin->key) {
                std::swap(curr->key, cousin->key);
                restoreHeapOrder(std::less<int>());
                parent = findParent_(cousinsLevel, cousinsLevel < currLevel ? (levelIndex>>1)^(maxNumberOfNodes_/2) : levelIndex);
                restoreHeapOrder(std::greater<int>());
            } else {
                restoreHeapOrder(std::less<int>());
            }
        } else {
            // node has been created inside the max heap
            if (curr->key < cousin->key) {
                std::swap(curr->key, cousin->key);
                restoreHeapOrder(std::greater<int>());
                parent = findParent_(cousinsLevel, levelIndex);
                restoreHeapOrder(std::less<int>());
            } else {
                restoreHeapOrder(std::greater<int>());
            }
        }
    }
    if (++levelIndex_ >= maxNumberOfNodes_) {
        levelIndex_ = 0;
        ++currentLevel_;
        maxNumberOfNodes_ *= 2;
    }
}
void DHeap::InsertFew(const size_t& x) {
    RandomInt random(LOWER_RANDOMIZATION_BRINK, UPPER_RANDOMIZATION_BRINK);
    for (int i = 0; i < x; ++i) {
        Insert(random());
    }
}
int DHeap::deleteHighestNode_(Node*& root, const std::function<bool(const int&, const int&)>& predicate) {
    if (root == nullptr) {
        return 0;
    }
    // searching for the most-rightly set leaf
    unsigned long levelIndex = levelIndex_;
    unsigned long currentLevel = currentLevel_;
    if (levelIndex == 0) {
        currentLevel--;
        maxNumberOfNodes_ /= 2;
    }
    levelIndex = ((currentLevel != currentLevel_) ? (maxNumberOfNodes_-1) : (levelIndex_-1));
    levelIndex_ = levelIndex;
    currentLevel_ = currentLevel;
    
    Node** current = &root_;
    Node** prev = nullptr;
    for (size_t i = maxNumberOfNodes_>>1; i; i>>=1) {
        prev = current;
        if (levelIndex & i) {
            current = &((*current)->right);
        } else {
            current = &((*current)->left);
        }
    }
    current = (*current == nullptr ? prev : current);
    
    int result = root->key;
    std::swap(root->key, (*current)->key);
    delete *current;
    *current = nullptr;
    
    Node* itr = root;
    Node* previous = nullptr;
    levelIndex = (root == root_->left ? 0 : 1);
    currentLevel = 1;
    
    while (itr) {
        previous = itr;
        if (!itr->left && !itr->right) {
            break;
        } else if (!itr->right) {
            if (!predicate(itr->key, itr->left->key) && (itr->key != itr->left->key)) {
                std::swap(itr->left->key, itr->key);
                ++currentLevel;
                levelIndex <<= 1;
            }
            break;
        } else if (predicate(itr->right->key, itr->left->key)) {
            if (!predicate(itr->key, itr->right->key) && (itr->key != itr->right->key)) {
                std::swap(itr->right->key, itr->key);
                ++currentLevel;
                levelIndex <<= 1;
                levelIndex |= 1UL;
                
                itr = itr->right;
            } else {
                break;
            }
        } else {
            if (!predicate(itr->key, itr->left->key) && (itr->key != itr->left->key)) {
                std::swap(itr->left->key, itr->key);
                ++currentLevel;
                levelIndex <<= 1;
                
                itr = itr->left;
            } else {
                break;
            }
        }
    }
    unsigned long cousinsLevel = 0;
    Node* cousin = findCousin_(currentLevel, levelIndex, cousinsLevel);
    if (previous) {
        if (!(cousin->left) && !(cousin->right)) {
            if (predicate(cousin->key, previous->key)) {
                std::swap(cousin->key, previous->key);
            }
        } else if (!(cousin->right)) {
            cousin = cousin->left;
            if (predicate(cousin->key, previous->key)) {
                std::swap(cousin->key, previous->key);
            }
        } else {
            if (predicate(cousin->left->key, cousin->right->key)) {
                cousin = cousin->left;
            } else {
                cousin = cousin->right;
            }
            if (predicate(cousin->key, previous->key)) {
                std::swap(cousin->key, previous->key);
            }
        }
    }
    return result;
}
int DHeap::DeleteMaxNode() {
    return deleteHighestNode_(root_->right, std::greater<int>());
}
int DHeap::DeleteMinNode() {
    return deleteHighestNode_(root_->left, std::less<int>());
}
void DHeap::ShowPreorder() const {
    unsigned numberOfVisitedNodes = 0U;
    std::function<void(Node*, unsigned int&)> preorder_lambda = [&] (Node* current, unsigned int& numberOfVisitedNodes) {
        if (current) {
            std::cout << current->key << " ";
            ++numberOfVisitedNodes;
            preorder_lambda(current->left, numberOfVisitedNodes);
            preorder_lambda(current->right, numberOfVisitedNodes);
        }
    };
    std::cout << "**************************************************************"
                 "\nWyswietlam drzewo w trybie preorder:"
                 "\n______________________________________________________________"
                 "\nCzesc kopca zorientowana na minimalna wartosc:"
                 "\n______________________________________________________________" << std::endl;
    preorder_lambda(root_->left, numberOfVisitedNodes);
    std::cout << "\n______________________________________________________________"
                 "\nCzesc kopca zorientowana na maksymalna wartosc:"
                 "\n______________________________________________________________" << std::endl;
    preorder_lambda(root_->right, numberOfVisitedNodes);
    std::cout << "\n--------------------------------------------------------------"
                 "\nLiczba odwiedzonych wezlow: " << numberOfVisitedNodes <<
                 "\n**************************************************************" << std::endl;
}
void DHeap::Cleanup() {
    std::function<void(Node*&)> cleanup = [&] (Node*& current) {
        if (current->left) {
            cleanup(current->left);
        }
        if (current->right) {
            cleanup(current->right);
        }
        delete current;
        current = nullptr;
    };
    if (root_) {
        cleanup(root_);
    }
}
Node* DHeap::findCousin_(const unsigned long& level, unsigned long index, unsigned long& cousinsLevelCallback) {
    Node* previous = nullptr;
    Node* current = root_;
    unsigned long maxNumberOfNodes = 1;
    for (unsigned long i = 0; i < level-1; ++i) {
        maxNumberOfNodes *= 2;
    }
    // changing first bit to the opposite
    index ^= maxNumberOfNodes;
    for (size_t i = maxNumberOfNodes; i; i>>=1) {
        previous = current;
        if (index & i) {
            current = current->right;
        } else {
            current = current->left;
        }
    }
    return current ? (cousinsLevelCallback=level, current) : (cousinsLevelCallback=level-1, previous);
}
Node* DHeap::findParent_(const unsigned long& level, unsigned long index) {
    Node* current = root_;
    unsigned long maxNumberOfNodes = 1;
    for (unsigned long i = 0; i < level-1; ++i) {
        maxNumberOfNodes *= 2;
    }
    index >>= 1;
    for (size_t i = maxNumberOfNodes>>1; i; i>>=1) {
        if (index & i) {
            current = current->right;
        } else {
            current = current->left;
        }
    }
    return current;
}
