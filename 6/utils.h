// utils.h

#include <iostream>
#include <cstdlib>
#include <vector>

#ifndef utils_h
#define utils_h

struct Node {
    Node() {}
    Node(const int& k) : key{k} {
        data = new char[DATA_SIZE];
        for (size_t i = 0U; i < Node::DATA_SIZE; ++i) {
            data[i] = static_cast<char>(rand() % (255 + 1));
        }
    }
    ~Node() {
        if (data) {
            delete[] data;
            data = nullptr;
        }
    }
    static const size_t DATA_SIZE = 16;
    
    char* data;
    int key {0};
    Node* left {nullptr};
    Node* right {nullptr};
};

enum class State {
    Success,
    Failure
};

class RandomInt {
public:
    RandomInt() = delete;
    RandomInt(const int& a, const int& b) : begin{a}, end{b} {}
    int operator()() {
        bool foundAlreadyRandomized = true;
        int newRandom = 0;
        
        while (foundAlreadyRandomized) {
            newRandom = rand() % (end - begin + 1) + begin;
            auto i = alreadyRandomized.begin();
            for (; i != alreadyRandomized.end(); ++i) {
                if (*i == newRandom) {
                    break;
                }
            }
            if (i == alreadyRandomized.end()) {
                alreadyRandomized.push_back(newRandom);
                break;
            }
        }
        return newRandom;
    }
private:
    std::vector<int> alreadyRandomized;
    int begin, end;
};

#endif
