// bst.cpp

#include "bst.h"
using std::cout;
using std::endl;

BinarySearchTree::BinarySearchTree() : root{nullptr} {
    
}
BinarySearchTree::~BinarySearchTree() {
    cleanup(root);
}

State BinarySearchTree::Insert(const int& key) {
    auto fillNode = [] (Node* node) {
        RandomInt random(0, 255);
        for (char& i : node->data) {
            i = static_cast<char>(random());
        }
    };
    
    if (!root) {
        root = new Node(key);
        fillNode(root);
    } else {
        Node* current = root;
        bool found = false;
        
        auto handleNode = [&] (Node*& next) {
            if (!next) {
                next = new Node(key);
                fillNode(next);
                found = true;
            } else {
                current = next;
            }
            
        };
        while (!found) {
            if (key > current->key) {
                handleNode(current->rightDesc);
            } else if (key < current->key){
                handleNode(current->leftDesc);
            } else {
                return State::Failure;
            }
        }
    }
    return State::Success;
}

void BinarySearchTree::InsertFew(const int& x) {
    RandomInt random(11, 19000);
    for (int i = 0; i < x; ++i) {
        Insert(random());
    }
}

State BinarySearchTree::Delete(const int& key) {
    if (!root) {
        return State::Failure;
    }
    Node* wanted = root;
    Node* parent = nullptr;
    bool found = false;
    
    auto clearPointer = [] (Node*& pointer) {
        if (pointer) {
            delete pointer;
            pointer = nullptr;
        }
    };
    
    auto setDescendants = [&] (Node* setTo) {
        if (parent->leftDesc == wanted) {
            parent->leftDesc = setTo;
            clearPointer(wanted);
        } else {
            parent->rightDesc = setTo;
            clearPointer(wanted);
        }
    };
    
    while (wanted && !found) {
        if (key == wanted->key) {
            found = true;
        } else {
            parent = wanted;
            if (key > wanted->key) {
                wanted = wanted->rightDesc;
            } else if (key < wanted->key) {
                wanted = wanted->leftDesc;
            }
        }
    }
    if (!wanted) {
        return State::Failure;
    }
    // leaf
    if (!wanted->leftDesc && !wanted->rightDesc) {
        if (wanted == root) {
            clearPointer(root);
        } else {
            setDescendants(nullptr);
        }
    } else if (!wanted->rightDesc) {
        if (wanted == root) {
            Node* newRoot = root->leftDesc;
            delete root;
            root = newRoot;
        } else {
            setDescendants(wanted->leftDesc);
        }
    } else if (!wanted->leftDesc) {
        if (wanted == root) {
            Node* newRoot = root->rightDesc;
            delete root;
            root = newRoot;
        } else {
            setDescendants(wanted->rightDesc);
        }
    } else {
        // 2nd degree subtree
        Node* successor = wanted->rightDesc;
        Node* parentOfSuccessor = wanted;
        Node* minimumNode = getTreeMinimum(wanted->rightDesc);
        
        if (successor != minimumNode) {
            while (successor != minimumNode) {
                parentOfSuccessor = successor;
                successor = successor->leftDesc;
            }
        }
        if (wanted == root) {
            if (successor->rightDesc) {
                parentOfSuccessor->leftDesc = successor->rightDesc;
            } else {
                parentOfSuccessor->leftDesc = nullptr;
            }
            successor->rightDesc = root->rightDesc;
            successor->leftDesc = root->leftDesc;
            clearPointer(root);
            root = successor;
        } else {
            if (successor->rightDesc) {
                parentOfSuccessor->rightDesc = successor->rightDesc;
            } else {
                if (parentOfSuccessor != wanted) {
                    parentOfSuccessor->leftDesc = nullptr;
                }
            }
            if (parent->leftDesc == wanted) {
                parent->leftDesc = successor;
            } else {
                parent->rightDesc = successor;
            }
            successor->leftDesc = wanted->leftDesc;
            clearPointer(wanted);
        }
    }
    return State::Success;
}

Node* BinarySearchTree::Search(const int& key) const {
    Node* current = root;
    while (current) {
        if (key > current->key) {
            current = current->rightDesc;
        } else if (key < current->key) {
            current = current->leftDesc;
        } else {
            return current;
        }
    }
    return nullptr;
}


void BinarySearchTree::ShowPreorder() const {
    unsigned numberOfVisitedNodes = 0U;
    cout << "----------------------------------------------------------" << endl
    << "Wyswietlam drzewo w trybie preorder:\n";
    showPreorder(root, numberOfVisitedNodes);
    cout << "\nLiczba odwiedzonych wezlow: " << numberOfVisitedNodes << endl
    << "----------------------------------------------------------" << endl;
}
void BinarySearchTree::ShowInorder() const {
    unsigned numberOfVisitedNodes = 0U;
    cout << "----------------------------------------------------------" << endl
    << "Wyswietlam drzewo w trybie inorder:\n";
    showInorder(root, numberOfVisitedNodes);
    cout << "\nLiczba odwiedzonych wezlow: " << numberOfVisitedNodes << endl
    << "----------------------------------------------------------" << endl;
}
void BinarySearchTree::ShowPostorder() const {
    unsigned numberOfVisitedNodes = 0U;
    cout << "Wyswietlam drzewo w trybie postorder:\n";
    showPostorder(root, numberOfVisitedNodes);
    cout << "\nLiczba odwiedzonych wezlow: " << numberOfVisitedNodes << endl;
}
void BinarySearchTree::showPreorder(Node* x, unsigned& numberOfVisitedNodes) const {
    if (x) {
        cout << x->key << ' ';
        ++numberOfVisitedNodes;
        showPreorder(x->leftDesc, numberOfVisitedNodes);
        showPreorder(x->rightDesc, numberOfVisitedNodes);
    }
}
void BinarySearchTree::showInorder(Node* x, unsigned& numberOfVisitedNodes) const {
    if (x) {
        showInorder(x->leftDesc, numberOfVisitedNodes);
        cout << x->key << ' ';
        ++numberOfVisitedNodes;
        showInorder(x->rightDesc, numberOfVisitedNodes);
    }
}
void BinarySearchTree::showPostorder(Node* x, unsigned& numberOfVisitedNodes) const {
    if (x) {
        showPostorder(x->leftDesc, numberOfVisitedNodes);
        showPostorder(x->rightDesc, numberOfVisitedNodes);
        cout << x->key << ' ';
        ++numberOfVisitedNodes;
    }
}
void BinarySearchTree::cleanup(Node* x) {
    if (x) {
        Node* right = x->rightDesc;
        if (x) {
            cleanup(x->leftDesc);
        }
        delete x;
        x = nullptr;
        if (right) {
            cleanup(right);
        }
        
    }
}
Node* BinarySearchTree::getTreeMinimum(Node* x) const {
    while (x->leftDesc) {
        x = x->leftDesc;
    }
    return x;
}
