// bst.h

#ifndef bst_h
#define bst_h

#include "util.h"

class BinarySearchTree {
public:
    BinarySearchTree();
    BinarySearchTree(const BinarySearchTree&) = delete;
    BinarySearchTree& operator=(const BinarySearchTree&) = delete;
    ~BinarySearchTree();
    
    State Insert(const int&);
    void InsertFew(const int&);
    Node* Search(const int&) const;
    State Delete(const int&);
    void ShowPreorder() const;
    void ShowInorder() const;
    void ShowPostorder() const;
    
private:
    Node* root;

    void cleanup(Node*);
    void showPreorder(Node*, unsigned&) const;
    void showInorder(Node*, unsigned&) const;
    void showPostorder(Node*, unsigned&) const;
    Node* getTreeMinimum(Node*) const;
    Node* getSuccessor(Node*) const;
};
#endif
