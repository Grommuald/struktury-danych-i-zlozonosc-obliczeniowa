// main.cpp

#include "bst.h"
#include <fstream>

int main()
{
    using namespace std;
    using namespace chrono;
    
    ifstream inputFile("inlab03.txt");
    if (inputFile.is_open()) {
        int x = 0;
        inputFile >> x;
        int k[4] {};
        for (int& i : k) {
            inputFile >> i;
        }
        srand(static_cast<unsigned>(time(0)));

        auto t1 = high_resolution_clock::now();
        BinarySearchTree* bst = new BinarySearchTree();
        bst->Delete(k[0]);
        bst->Insert(k[0]);
        bst->InsertFew(x);
        bst->ShowInorder();
        bst->ShowPreorder();
        bst->Insert(k[1]);
        bst->ShowInorder();
        bst->Insert(k[2]);
        bst->Insert(k[3]);
        bst->Delete(k[0]);
        bst->ShowPreorder();
        Node* searched = bst->Search(k[0]);
        if (searched) {
            cout << "Znaleziono element o wartosci: " << k[0] << endl;
        } else {
            cout << "Nie znaleziono element o wartosci: " << k[0] << endl;
        }
        bst->Delete(k[1]);
        bst->ShowInorder();
        bst->Delete(k[2]);
        bst->Delete(k[3]);
        auto t2 = high_resolution_clock::now();
        cout << "Czas wykonania: " << duration_cast<milliseconds>(t2-t1).count() << " ms." << endl;
        delete bst;
    } else {
        cout << "Nie udalo sie otworzyc pliku.\n";
    }
    return 0;
}
