// util.h

#ifndef util_h
#define util_h

#include <iostream>
#include <ctime>
#include <vector>
#include <random>

struct Node {
    Node(const int& k) : key{k}, leftDesc{nullptr}, rightDesc{nullptr} {}
    
    char data[100] {};
    int key;
    Node* leftDesc;
    Node* rightDesc;
};

enum class State {
    Success,
    Failure
};

class RandomIntShuffle {
public:
    RandomIntShuffle(const int& a, const int& b) {
        int length = b-a;
        for (int i = 0; i < length; ++i) {
            randomizationVector.push_back(a+i);
        }
        std::random_device rd;
        std::mt19937 g(rd());
        
        std::shuffle(randomizationVector.begin(), randomizationVector.end(), g);
    }
    int operator()() {
        return randomizationVector[vectorIndex++];
    }
private:
    RandomIntShuffle() {}
    std::vector<int> randomizationVector;
    int vectorIndex {};
};

class RandomInt {
public:
    RandomInt(const int& a, const int& b) : begin{a}, end{b} {}
    int operator()() {
        bool foundAlreadyRandomized = true;
        int newRandom = 0;
        
        while (foundAlreadyRandomized) {
            newRandom = rand() % (end - begin + 1) + begin;
            auto i = alreadyRandomized.begin();
            for (; i != alreadyRandomized.end(); ++i) {
                if (*i == newRandom) {
                    break;
                }
            }
            if (i == alreadyRandomized.end()) {
                alreadyRandomized.push_back(newRandom);
                break;
            }
        }
        return newRandom;
    }
private:
    RandomInt() {}
    
    std::vector<int> alreadyRandomized;
    int begin, end;
};
#endif
