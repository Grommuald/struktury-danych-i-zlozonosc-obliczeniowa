// splay_tree.hpp

#include <vector>

#ifndef splay_tree_hpp
#define splay_tree_hpp

struct Node;
enum class State;
typedef unsigned long size_t;

class SplayTree {
public:
    SplayTree();
    SplayTree(const SplayTree&) = delete;
    SplayTree& operator=(const SplayTree&) = delete;
    ~SplayTree();
    
    State Insert(const int&);
    State Delete(const int&);
    State Cleanup();
    Node* Search(const int&);
    void InsertFew(const size_t&);
    void ShowInorder() const;
    void ShowPreorder() const;
private:
    Node* root_;
    
    static const int UPPER_RANDOMIZATION_BRINK;
    static const int LOWER_RANDOMIZATION_BRINK;
    
    void promoteToRoot(std::vector<Node*>&);
    inline Node* getTreeMinimum(Node*) const;
    inline Node* getTreeMaximum(Node*) const;
};
#endif
