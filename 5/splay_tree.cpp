// splay_tree.cpp

#include "utils.h"
#include "splay_tree.hpp"
#include <iostream>
using namespace std;

const int SplayTree::UPPER_RANDOMIZATION_BRINK = 30000;
const int SplayTree::LOWER_RANDOMIZATION_BRINK = 24;

SplayTree::SplayTree() : root_(nullptr) {
    
}
SplayTree::~SplayTree() {
    Cleanup();
}
State SplayTree::Cleanup() {
    std::function<void(Node*&)> cleanup = [&] (Node*& current) {
        if (current->left) {
            cleanup(current->left);
        }
        if (current->right) {
            cleanup(current->right);
        }
        delete current;
        current = nullptr;
    };
    if (root_) {
        cleanup(root_);
        return State::Success;
    } else {
        return State::Failure;
    }
}
State SplayTree::Insert(const int& key) {
    if (!root_) {
        root_ = new Node(key);
    } else {
        Node* current = root_;
        bool found = false;
        std::vector<Node*> track {current};
        
        auto handleNode = [&] (Node*& next) {
            if (!next) {
                next = new Node(key);
                found = true;
            } else {
                current = next;
            }
            track.push_back(next);
        };
        while (!found) {
            if (key > current->key) {
                handleNode(current->right);
            } else if (key < current->key){
                handleNode(current->left);
            } else {
                return State::Failure;
            }
        }
        promoteToRoot(track);
    }
    return State::Success;
}

void SplayTree::InsertFew(const size_t& x) {
    RandomInt random(LOWER_RANDOMIZATION_BRINK, UPPER_RANDOMIZATION_BRINK);
    for (int i = 0; i < x; ++i) {
        Insert(random());
    }
}
Node* SplayTree::Search(const int& key) {
    Node* current = root_;
    std::vector<Node*> track;
    while (current) {
        track.push_back(current);
        if (key > current->key) {
            current = current->right;
        } else if (key < current->key) {
            current = current->left;
        } else {
            promoteToRoot(track);
            return current;
        }
    }
    return nullptr;
}
State SplayTree::Delete(const int& key) {
    if (!root_) {
        return State::Failure;
    }
    Node* wanted = root_;
    Node* parent = nullptr;
    bool found = false;
    std::vector<Node*> track;
    
    auto clearPointer = [] (Node*& pointer) {
        if (pointer) {
            delete pointer;
            pointer = nullptr;
        }
    };
    while (wanted && !found) {
        track.push_back(wanted);
        if (key == wanted->key) {
            found = true;
        } else {
            parent = wanted;
            if (key > wanted->key) {
                wanted = wanted->right;
            } else if (key < wanted->key) {
                wanted = wanted->left;
            }
        }
    }
    if (!wanted) {
        return State::Failure;
    }
    // promote wanted node to the root
    promoteToRoot(track);
    
    Node* successor = wanted->right;
    Node* parentOfSuccessor = wanted;
    Node* minimumNode = getTreeMinimum(wanted->right);
        
    if (successor != minimumNode) {
        while (successor != minimumNode) {
            parentOfSuccessor = successor;
            successor = successor->left;
        }
    }
    if (wanted == root_) {
        if (successor->right) {
            parentOfSuccessor->left = successor->right;
        } else {
            parentOfSuccessor->left = nullptr;
        }
        successor->right = root_->right;
        successor->left = root_->left;
        clearPointer(root_);
        root_ = successor;
    }
    return State::Success;
}
void SplayTree::ShowInorder() const {
    unsigned numberOfVisitedNodes = 0U;
    cout << "----------------------------------------------------------" << endl
    << "Wyswietlam drzewo w trybie inorder:\n";
    std::function<void(Node*, unsigned int&)> inorder_lambda = [&] (Node* current, unsigned int& numberOfVisitedNodes) {
        if (current) {
            inorder_lambda(current->left, numberOfVisitedNodes);
            cout << current->key << " ";
            ++numberOfVisitedNodes;
            inorder_lambda(current->right, numberOfVisitedNodes);
        }
    };
    inorder_lambda(root_, numberOfVisitedNodes);
    cout << "\nLiczba odwiedzonych wezlow: " << numberOfVisitedNodes << endl
    << "---------------------------------------------------------------" << endl;
}
void SplayTree::ShowPreorder() const {
    unsigned numberOfVisitedNodes = 0U;
    cout << "----------------------------------------------------------" << endl
    << "Wyswietlam drzewo w trybie preorder:\n";
    std::function<void(Node*, unsigned int&)> preorder_lambda = [&] (Node* current, unsigned int& numberOfVisitedNodes) {
        if (current) {
            cout << current->key << " ";
            ++numberOfVisitedNodes;
            preorder_lambda(current->left, numberOfVisitedNodes);
            preorder_lambda(current->right, numberOfVisitedNodes);
        }
    };
    preorder_lambda(root_, numberOfVisitedNodes);
    cout << "\nLiczba odwiedzonych wezlow: " << numberOfVisitedNodes << endl
    << "---------------------------------------------------------------" << endl;
}
void SplayTree::promoteToRoot(std::vector<Node*>& track) {
    // promotes the last node on the track to the root
    for (size_t i = track.size() - 1; i >= 2; --i) {
        if (track[i] == track[i-1]->left) {
            if (track[i-1] == track[i-2]->right) {
                // zig-zag
                track[i-2]->right = track[i];
            } else {
                // zig-zig
                track[i-2]->left = track[i];
            }
            track[i-1]->left = track[i]->right;
            track[i]->right = track[i-1];
        } else {
            if (track[i-1] == track[i-2]->right) {
                // zig-zig
                track[i-2]->right = track[i];
            } else {
                // zig-zag
                track[i-2]->left = track[i];
            }
            track[i-1]->right = track[i]->left;
            track[i]->left = track[i-1];
        }
        swap(track[i], track[i-1]);
    }
    // swaping 2nd degree node with the root
    if (track[0]->left == track[1]) {
        track[0]->left = track[1]->right;
        track[1]->right = track[0];
    } else {
        track[0]->right = track[1]->left;
        track[1]->left = track[0];
    }
    root_ = track[1];
}
Node* SplayTree::getTreeMinimum(Node* x) const {
    while (x->left) {
        x = x->left;
    }
    return x;
}
Node* SplayTree::getTreeMaximum(Node* x) const {
    while (x->right) {
        x = x->right;
    }
    return x;
}
