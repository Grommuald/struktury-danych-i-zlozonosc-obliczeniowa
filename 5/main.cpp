// main.cpp

#include "utils.h"
#include "splay_tree.hpp"
#include <iostream>
#include <fstream>
#include <chrono>

int main(int argc, const char * argv[]) {
    using namespace std;
    using namespace chrono;
    
    ifstream inputFile("inlab05.txt");
    if (inputFile.is_open()) {
        int x;
        inputFile >> x;
        
        int k[3] {};
        for (int& i : k) {
            inputFile >> i;
        }
        srand(static_cast<unsigned>(time(0)));
        
        auto t1 = high_resolution_clock::now();
        SplayTree* splay = new SplayTree();
        splay->InsertFew(x);
        splay->ShowInorder();
        splay->ShowPreorder();
        
        int replacedK = k[0];
        while (splay->Insert(replacedK++) == State::Failure)
            ;
        splay->ShowPreorder();
        replacedK = k[1];
        while (splay->Insert(replacedK++) == State::Failure)
            ;
        splay->ShowPreorder();
        
        Node* found = nullptr;
        if ((found = splay->Search(k[2]))) {
            cout << "Znaleziono wezel o kluczu: " << found->key << endl;
        } else {
            cout << "Nie znaleziono wezla o kluczu: " << k[2] << endl;
        }
        splay->ShowPreorder();
        if (splay->Delete(k[1]) == State::Success) {
            cout << "Wezel, o kluczu: " << k[1] << ", zostal usuniety." << endl;
        } else {
            cout << "Wezel, o kluczu: " << k[1] << ", nie zostal usuniety." << endl;
        }
        splay->ShowPreorder();
        splay->ShowInorder();
        auto t2 = high_resolution_clock::now();
        cout << "Czas wykonania: " << duration_cast<milliseconds>(t2-t1).count() << " ms." << endl;
        delete splay;
    } else {
        cout << "Nie udalo sie otworzyc pliku.\n";
    }
    return 0;
}
