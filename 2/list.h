// list.h

#ifndef list_h
#define list_h

#include "util.h"

class CircularList {
public:
    CircularList();
    ~CircularList();
    CircularList(const CircularList&) = delete;
    CircularList& operator=(const CircularList&) = delete;
    
    State PrintKeys() const;
    State PrintFirstFew(size_t) const;
    State Add(const int&);
    void AddFew(const size_t&);
    Node* Search(const int&) const;
    State Delete(const int&);
    void Erase();
    
private:
    Node* head;
    Node* GetTail() const;
};
#endif
