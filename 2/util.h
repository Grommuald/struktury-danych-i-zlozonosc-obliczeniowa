// util.h

#ifndef util_h
#define util_h

#include <iostream>
#include <vector>
#include <algorithm>
#include <random>

struct Node {
    Node(const int& key, const double& dval, const char& cval) {
        this->key = key;
        dVal = dval;
        cVal = cval;
        next = nullptr;
    }
    int key;
    double dVal;
    char cVal;
    Node* next;
};

enum class State {
    Success,
    Failure
};

class RandomIntShuffle {
public:
    RandomIntShuffle(const int& a, const int& b) {
        int length = b-a;
        for (int i = 0; i < length; ++i) {
            randomizationVector.push_back(a+i);
        }
        std::mt19937 mt {std::random_device{}()};
        std::shuffle(randomizationVector.begin(), randomizationVector.end(), mt);
    }
    int operator()() {
        return randomizationVector[vectorIndex++];
    }
private:
    RandomIntShuffle() {}
    std::vector<int> randomizationVector;
    int vectorIndex {};
};

#endif
