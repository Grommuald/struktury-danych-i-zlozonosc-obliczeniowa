
// main.cpp

#include "list.h"
#include <fstream>
#include <chrono>
#include <iostream>
using namespace std;
using namespace std::chrono;

int main()
{
    ifstream inputFile("inlab02.txt");
    
    size_t x;
    int k[5] = {};
    if (inputFile.is_open()) {
        inputFile >> x;
        
        for (int& i : k) {
            inputFile >> i;
        }
        srand(static_cast<unsigned>(time(0)));
        
        auto t0 = high_resolution_clock::now();
        CircularList* list = new CircularList();
        list->Add(k[0]);
        list->AddFew(x);
        cout << "Prezentacja wartosci kluczowych 20 pierwszych wezlow: " << endl;
        list->PrintFirstFew(20);
        list->Add(k[1]);
        cout << "Prezentacja wartosci kluczowych 20 pierwszych wezlow: " << endl;
        list->PrintFirstFew(20);
        list->Add(k[2]);
        cout << "Prezentacja wartosci kluczowych 20 pierwszych wezlow: " << endl;
        list->PrintFirstFew(20);
        list->Add(k[3]);
        cout << "Prezentacja wartosci kluczowych 20 pierwszych wezlow: " << endl;
        list->PrintFirstFew(20);
        list->Add(k[4]);
        list->Delete(k[2]);
        cout << "Prezentacja wartosci kluczowych 20 pierwszych wezlow: " << endl;
        list->PrintFirstFew(20);
        list->Delete(k[1]);
        cout << "Prezentacja wartosci kluczowych 20 pierwszych wezlow: " << endl;
        list->PrintFirstFew(20);
        list->Delete(k[4]);
        Node* searched = list->Search(k[4]);
        if (searched) {
            cout << "Znaleziono element o wartosci: " << k[4] << endl;
        } else {
            cout << "Nie znaleziono elementu wartosci: " << k[4] << endl;
        }
        list->Erase();
        auto t1 = high_resolution_clock::now();
        cout << "Czas wykonania: " << duration_cast<milliseconds>(t1-t0).count() << " ms." << endl;
        delete list;
    } else {
        cout << "Nie mozna otworzyc pliku." << endl;
    }
    inputFile.close();

    return 0;
}
