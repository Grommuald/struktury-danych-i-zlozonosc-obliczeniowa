// list.cpp

#include "list.h"
using std::cout;
using std::endl;

CircularList::CircularList() : head(nullptr) {
    
}
CircularList::~CircularList() {
    Erase();
}

State CircularList::PrintKeys() const {
    if (!head) {
        return State::Failure;
    }
    cout << head->key << " ";
    for (Node* i = head->next; i != head; i = i->next) {
        cout << i->key << " ";
    }
    cout << endl;
    return State::Success;
}

State CircularList::Add(const int& key) {
    Node* newNode = new Node(key, static_cast<double>(rand()) / RAND_MAX, 'Z');
    
    if (!head) {
        head = newNode;
        newNode->next = head;
        return State::Success;
    }
    Node* tail = GetTail();
    // the key value is less than head's key value
    if (key < head->key) {
        Node* oldHead = head;
        
        head = newNode;
        head->next = oldHead;
        tail->next = head;
        return State::Success;
    }
    // the key value is greater than tail's key value
    if (key > tail->key) {
        tail->next = newNode;
        tail = tail->next;
        tail->next = head;
        return State::Success;
    }
    // the key value is somewhere in between the head and tail
    Node* i = head;
    
    while (i->next != head) {
        if (i->key == key) {
            return State::Failure;
        }
        if (key > i->key && key < i->next->key) {
            Node* next = i->next;
            i->next = newNode;
            newNode->next = next;
            return State::Success;
        }
        i = i->next;
    }
    return State::Success;
}

void CircularList::AddFew(const size_t& x) {
    RandomIntShuffle randomizeInt(10, 20000);
    for (size_t i = 0; i < x; ++i) {
        Add(randomizeInt());
    }
}

Node* CircularList::Search(const int& x) const {
    if (!head) {
        return nullptr;
    }
    Node* i = head;
    while (i->next != head) {
        if (i->key == x) {
            return i;
        }
        i = i->next;
    }
    // checking the last node
    return i->key == x ? i : nullptr;
}

State CircularList::Delete(const int& key) {
    if (!head) {
        return State::Failure;
    }
    Node* found = Search(key);
    if (!found) {
        return State::Failure;
    }
    // now we know the node is definitely on the list
    // only one node
    if (head->next == head) {
        delete head;
    } else {
        // more than one node
        if (head == found) {
            Node* newHead = head->next;
            GetTail()->next = newHead;
            delete head;
            head = newHead;
        } else {
            Node* i = head->next;
            while (i->next != found)
                i = i->next;
            i->next = found->next;
            delete found;
        }
    }
    return State::Success;
}

State CircularList::PrintFirstFew(size_t y) const {
    if (!head) {
        return State::Failure;
    }
    
    if (y > 0) {
        size_t currentLength = 1U;
        // checking if length is less or equal to the length of list
        for (Node* i = head->next; i != head; i = i->next) {
            ++currentLength;
        }
        if (y > currentLength) {
            return State::Failure;
        }
        y--;
        cout << head->key << " ";
        
        Node* i = head->next;
        while (i != head && y-- > 0) {
            cout << i->key << " ";
            i = i->next;
        }
    }
    cout << endl;
    return State::Success;
}

void CircularList::Erase()
{
    if (head) {
        // the list has only one node
        if (head->next == head) {
            delete head;
            head = nullptr;
        } else {
            // the list has more than one node
            Node* i = head->next;
            while (i != head) {
                Node* next = i->next;
                delete i;
                i = next;
            }
            delete i;
        }
    }
    head = nullptr;
}

Node* CircularList::GetTail() const {
    if (!head) {
        return nullptr;
    }
    Node* tail = head;
    while (tail->next != head) {
        tail = tail->next;
    }
    return tail;
}



